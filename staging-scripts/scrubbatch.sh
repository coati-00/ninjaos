#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# uses the "scrub" binary with multiple files at one. use as scrubbatch.sh <file1><file2><file3>....
FILELIST=($@)
LINES="$#"
PATTERN="random2"

##start by getting options
#help is first
if [ "$1" = "--help" ];then
   echo "$(tput bold)scrubbatch.sh$(tput sgr0): scrubbatch.sh uses the \'scrub\' binary to securely shred files. This is a simple bash script to do more than one at a time."
   echo ""
   echo " " $(tput bold)Usage: $(tput sgr0) scrubbatch.sh "<options> <files>"
   echo "$(tput bold)Options:$(tput sgr0)"
  echo "	$(tput bold)-p$(tput sgr0), $(tput bold)--pattern$(tput sgr0) The scrub pattern type, it can be any pattern scrub supports. options are: nnsa, bsi, fastold, gutmann, random, random2, schneier, pfitzner7, pfitzner33. random2\(two random passes\) is the default. see \'man scrub\' for more information."
  exit 2
 #error if command is run by itself
 elif [ -z "$1" ];then
   echo "$(tput bold)scrubbatch.sh$(tput sgr0): No files specified. See scrubbatch.sh --help"
  exit 1
 elif [ $1 = "--pattern" ] || [ $1 = "-p" ];then
  FILELIST=`echo $FILELIST | cut -d ' ' -f 2-`
  PATTERN="$1"
 elif [ $1 = "--" ];then
  FILELIST=`echo $FILELIST | cut -d ' ' -f 2-`
fi

#now lets do the actual scrubbing, one at a time.
I=$(($LINES - 1))
FILE=""
while [ "$I" -ge "0" ];do
  $FILE=${FILELIST[$I]}
  png_strip $FILE
  EXIT=`expr $EXIT + $?`
  true $(($I--)
done

exit 0
