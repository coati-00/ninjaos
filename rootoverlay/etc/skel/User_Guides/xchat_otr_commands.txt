xChat OTR Commands:

/otr genkey nick@irc.server.com 
	Manually generate a key for the given account(also done on demand)
/otr auth [<nick>@<server>] <secret>
	Initiate or respond to an authentication challenge
/otr authabort [<nick>@<server>] 
	Abort any ongoing authentication
/otr trust [<nick>@<server>]
	Trust the fingerprint of the user in the current window.
	You should only do this after comparing fingerprints over a secure line
/otr debug
	Switch debug mode on/off
/otr contexts
	List all OTR contexts along with their fingerprints and status
/otr finish [<nick>@<server>]
	Finish an OTR conversation
/otr version
	Display irc-otr version. Might be a git commit

Settings:

otr_policy
	Comma-separated list of "<nick>@<server> <policy>" pairs. See comments
	above.
otr_policy_known
	Same syntax as otr_policy. Only applied where a fingerprint is
	available.
otr_ignore
	Conversations with nicks that match this regular expression completely
	bypass libotr. It is very unlikely that you need to touch this setting,
	just use the OTR policy never to prevent OTR sessions with some nicks.
otr_finishonunload
	If true running OTR sessions are finished on /unload and /quit.
otr_createqueries
	If true queries are automatically created for OTR log messages.

© 2011-13 Ninja OS. Licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)


