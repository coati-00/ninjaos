#!/bin/bash
# Written for Ninja OS, by the dev team. Dual licensed, your pick of GPL2 or GPL3 or later
# https://www.gnu.org/licenses/gpl-2.0.html
# https://www.gnu.org/licenses/gpl.html
# This script tests to make sure /var/log/privoxy exists, and if it does not, its created and owned by user and group "privoxy"
# Run as root. If $1 and $2 exist, they are treated as user and group respectively, the defaults are privoxy
EXIT="0"
if [ ! -z "$1" ];then
    user="$1"
  else
    user="privoxy"
fi
if [ ! -z "$2" ];then
    group="$2"
  else
    group="privoxy"
fi

if [ ! -d /var/log/privoxy ];then
   mkdir /var/log/privoxy
   EXIT=$((EXIT + $?))
   chown "$user":"$group" /var/log/privoxy
   EXIT=$((EXIT + $?))
fi
exit $EXIT
