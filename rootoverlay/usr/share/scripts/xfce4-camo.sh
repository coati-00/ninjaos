#!/bin/bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script disguises XFCE as another Operating System. use xfce4-camo.sh <OS-NAME>. Still experimental

if [ $1 == "winxp" ];then
    #Work in Proggress. Please note this does not work entirely as expected.
    xfconf-query -c xsettings -p /Net/ThemeName -s "Redmon"
    xfconf-query -c xfwm4 -p /general/theme -s "RedmonXP"
    xfconf-query -c xfce4-panel -p /panels/panel-0/autohide -s false
    xfconf-query -c xfce4-panel -p /plugins/plugin-1/button-icon -s "start_icon_xp"
    xfconf-query -c xfce4-panel -p /plugins/plugin-1/button-title -s "Start"
    echo "Windows XP Camouflage Work in Progress"
    exit 1
  elif [ $1 == "win7" ];then
    echo "Windows 7 Camouflage not implemented yet"
    exit 1
  elif [ $1 == "win8" ];then
    xfconf-query -c xfce4-panel -p /plugins/plugin-1/button-icon -s "start_icon8"
    echo "Windows 8 Camouflage not implemented yet"
    exit 1
  elif [ $1 == "osx" ];then
    echo "Mac OSX Camouflage not implemented yet"
    exit 1
  else
    echo "${0}:We don't have camouflauge for $1, valid options are \"winxp,win7,win8, and osx\""
    exit 1
fi

