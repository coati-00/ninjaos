#!/bin/bash
#
# Unmounts encrypted drives so the keys get wiped from memory.

#unmount encrypted filesystems
truecrypt -d

umount /dev/mapper/udisks* &> /dev/null
luksClose /dev/mapper/udisks* &> /dev/null

exit 0
