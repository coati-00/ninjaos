#!/bin/bash
. /usr/share/scripts/liveos_boilerplate.sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#This script makes a Ninja OS live usb drive from the boot sector and live .img files
#Define filenames
TARGET="$1"
MAINIMG="${OSSLUG}_${OSVERSION}.img"
BOOTSECTOR="ninjabootsector${OSVERSION}.img"
EXIT=0

get_target_console() {
    TARGET="none"
    echo "$(tput bold)makeninja.sh:$(tput sgr0) Please specificy target block device to install $OSNAME (in the format of /dev/sdX):"
    while [ ! -b $TARGET ];do
        read -p path: TARGET
        [ ! -b $TARGET ] && echo "$(tput bold)makeninja.sh:$(tput sgr0) $TARGET is not a valid block device, please try again in the format of (/dev/sdX):"
    done
    return
}
#Check to make sure we have a target, if not exit gracefully, and give help if asked.
if [ -z $1 ];then
  get_target_console
 elif [ $1 = "--help" ] || [ $1 = "-?" ]; then
  echo "$(tput bold)makeninja.sh:$(tput sgr0) This script makes a $OSNAME USB stick. You need both the main image, and the boot sector image in your current dirrectory. you need to partition the disk so the first partition is 1GB." 
  echo " "
  echo "type \"makeninja.sh /dev/sdX\" where sdX is you block device( i.e. sdb, sdc, sdd sde, etc...) "
 exit 1
fi

if [ ! -f "$MAINIMG" ];then
  echo "$(tput bold)$(tput setaf 1)makeninja.sh:$(tput sgr0) $MAINIMG not found in local directory, aborting."
  exit 1
 elif [ ! -f "$BOOTSECTOR" ];then
  echo "$(tput bold)$(tput setaf 1)makeninja.sh:$(tput sgr0) $BOOTSECTOR not found in local directory, aborting."
  exit 1
 elif [ ! -b "$TARGET" ];then
  echo "$(tput bold)$(tput setaf 1)makeninja.sh:$(tput sgr0) $TARGET is not a block device. Exiting..."
  exit 1
fi
#lets ask a sudo password now, rather than later
sudo true
#now we've checked, lets copy data to 
echo "$(tput bold)makeninja.sh:$(tput sgr0) copying ${OSNAME}(${OSVERSION}) system data to $TARGET. Be patient this might take a while..."

#start with the main image. if we can find "pv" on the system we use it for a status bar.
echo "		Copying $MAINIMG to ${TARGET}1:"
if [ -f $(which pv) ];then
  sudo dd if=$MAINIMG bs=128k 2> /dev/null | pv -B 128k -s 1222639616 | sudo dd of=${TARGET}1 bs=128k &> /dev/null
  EXIT=$(( $EXIT + $?))
 else
  sudo dd if=$MAINIMG of=$TARGET"1" bs=128k
  EXIT=$(( $EXIT + $?))
fi

#Hack to make sure that kernels 3.6.0 and later in conjunction with a auto mount tendencies don't leave the disk mounted before we write the boot sector. this results in a corrupt disk.
#sudo umount ${TARGET}* &> /dev/null

#Now we make the bootsector
echo "		Copying $BOOTSECTOR to ${TARGET}:"
if [ -f $(which pv) ];then
  sudo dd if=$BOOTSECTOR bs=440 count=1 2> /dev/null | pv -s 440 | sudo dd of=$TARGET bs=440 count=1 &> /dev/null
  EXIT=$(( $EXIT + $?))
 else
  sudo dd if=$BOOTSECTOR of=$TARGET bs=440 count=1
  EXIT=$(( $EXIT + $?))
fi

#Sync and exit according to status
sync
if [ "$EXIT" -eq "0" ];then
  echo "$(tput bold)$(tput setaf 6)DONE!$(tput sgr0)"
  exit 0
 else
  echo "$(tput bold)$(tput setaf 1)!FAIL!:$(tput sgr0)There was an error copying"
  exit $EXIT
fi
