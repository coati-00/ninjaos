#!/bin/bash
. /usr/share/scripts/liveos_boilerplate.sh
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Self Destruct/Zeroize. Do the UnThinkable. Wipe all data on USB stick, then reboot.
#
#First it wipes the first 128k(all potential encrypted partition headers) from every partition of the drive($RAND_SRC), making recovery of encrypted partions along with all filesystem tables, nigh impossible.
#Second, it zero fills the entire drive with $FILL_SRC.
#Third, and final, it re-partitions and reformats the disk as a fat32 partition, and reboots

FILL_SRC=/dev/zero
RAND_SRC=/dev/urandom
NOCONFIRM="NO"
SURE="no"
GUI="YES"
EXIT=""
if [[ $(cat /proc/cmdline) == *selfdestruct* ]] || [[ $(cat /proc/cmdline) == *zeroize* ]] || [[ $(cat /proc/cmdline) == *zzz* ]];then
    touch /tmp/bootmode
    NOCONFIRM="YES"
fi

if [[ ${@} == *--noconfirm* ]];then
    NOCONFIRM="YES"
fi

set_background() {
    tput setab 1
    tput setaf 8
    tput bold
}
are_you_sure() {
    if [ $NOCONFIRM == "YES" ];then
        SURE="y"
    else
        echo "ARE YOU SURE?"
        read -p \(yes/no\) SURE
    fi
}
#wipe the first 128k off every partition of the boot device
wipe_part_headers() {
    declare -a parts
    parts="$(ls ${BOOTDEV}?)"
    for part in $parts;do
        /tmp/emergency_bin/busybox dd if="$RAND_SRC" bs=128k count=1 of="$part" 2> /dev/null 
    done
    /tmp/emergency_bin/busybox sync
}
scrub_disk() {
    #This is the dirty work, lets start by trapping the escape commands, changing to virtual terminal 2, stopping the only other running virtual terminal(tty1), and disabling logins by locking root and making a blank file at /etc/nologin. This secures the console, making the machine impossible to access, and self destruct sequence impossible to abort, or otherwise gather any visual information about the running system. Also disables any and all network connections
    trap "finale" 1 2 9 15 17 19 23
    sudo chvt 1
    sudo systemctl stop NetworkManager &
    sudo systemctl stop bluetooth &
    sudo systemctl stop getty@tty2.service &
    sudo usermod -L root &
    sudo touch /etc/nologin &
    #Clear the screen and remove all formating. This is so no one can see what the computer was doing before SD/Z
    clear
    tput sgr0
    # Before we start, scrub RAM (not implemented, this doesn't work reliabily on modern systems)
    #sudo smem -llv 
    #unmount all encrypted paritions(clear them from memory, so they can't be recovered with a cold boot attack)
    sudo umount /dev/mapper/udisks* &> /dev/null
    sudo cryptsetup luksClose /dev/mapper/udisks* &> /dev/null
    #zuluMount -u
    #get the disk size, copy the needed binaries to ram based tmpfs on /tmp, and then chmod them SUID so they run as root.
    DISK_SIZE=$(sudo sfdisk -s ${BOOTDEV})
    sudo cp /var/emergency_bin/pv /tmp/emergency_bin/pv
    sudo chmod 6555 /tmp/emergency_bin/busybox
    sudo chmod 6555 /tmp/emergency_bin/pv
    #wipe the headers off all the paritions one by one BEFORE the zero fill, in case the proccess is interupted encrypted paritions are not recoverable.
    wipe_part_headers
    # Once you see Activated! on the screen, you can walk away safely, and all user data is unrecoverable. please note the ETA given below on the progress bar, which is how long a zerofill will take.
    echo "Please Stand By, Machine Will Reboot Upon Completion"
    # now we fill $BOOTDEV with zeros. $BOOTDEV is whatever device is mounted on /boot.
    ( /tmp/emergency_bin/busybox dd if=$FILL_SRC bs=128k 2> /dev/null | /tmp/emergency_bin/pv -pet -B 128k -s ${DISK_SIZE}k | /tmp/emergency_bin/busybox dd of=${BOOTDEV} bs=128k &> /dev/null; /tmp/emergency_bin/busybox sync )
    finale
}

finale() {
    # Reformat the disk with an MBR and a blank FAT32 partition. If this runs this far without interruption or being noticed, there is plausable deniability Ninja OS was ever on the disk to begin with. It will look like yet another blank USB Drive
    fdisk_final
    /tmp/emergency_bin/busybox mkfs.vfat ${BOOTDEV}
    /tmp/emergency_bin/busybox sync
    #reboot system when we are done. the "&" makes sure that reboot won't catch an interrupt from the console by running the reboot in the background.
    /tmp/emergency_bin/busybox reboot -f &
    #backstop to make the sure that this script won't return the user to an interactive shell, while waiting for reboot.
    while true ;do
       /tmp/emergency_bin/busybox sleep 2
    done

}

fdisk_final() {
/tmp/emergency_bin/busybox fdisk ${BOOTDEV} &> /dev/null << EOF
n
p
1


w
EOF
}
sd_abort() {
    echo "!Self Destruct/Zeroize Mode Aborted!"
    tput sgr0
    exit 1
}


#Should be self explanitory, give the user an option to abort. Flash in red and orange so they get the message something dangerous is about to happen.
sd_banner() {
    echo ""
    echo "	$(tput setaf 0) +$(tput setaf 3)---$(tput setaf 7)$OSNAME $(tput setaf 3)SELF $(tput  setaf 0)DESTRUCT$(tput setaf 3)---$(tput setaf 0)+$(tput sgr0)"
    set_background
    echo "	   SELF DESTRUCT/ZEROIZE: ACTIVATED"
    echo "!!!This script will wipe all data from the entire drive to include the operating system and all user data!!!"
}
#if we selected self destruct mode from the boot menu run this, or script is run with --noconfirm

#boot mode no longer has a menu, since it takes 28 seconds to get to are_you_sure, boot mode goes right to scrubbing no questions asked. simply add "selfdestruct" or "zeroize" to your cmdline manually at the boot screen. This seems pretty "cat stepped on the keyboard proof", and shortens the time until you can safely walk away from from a self destructing/zeroing flash disk.

[ -f /tmp/bootmode ] && scrub_disk

#Lets check how this script is run. In case you haven't noticed. This is some pretty insane flow control. It is a three stage script, with three possible start states, and one possible end state.
# Normally, the XFCE desktop is run on virtual terminal 1, started via .bash_profile, which in turn is set to autologin with mingetty. When the final phase is activated, a force logout is performed, which should kill user programs, and also kill X(and logout from tty1, if run from tty1). before this is done, this script is copied to .bash_profile, so when the system logs back in automaticly on tty1, it is run again. Once it detects that it is run as tty1, it can only know the self destruct sequence is started, and enters the final phase, the drive scrubbing.
if [ "$(tty)" != "/dev/tty1" ];then
    set_background
    sd_banner
    are_you_sure
    shopt -s nocasematch
    if [ "$SURE" == "yes" ] || [ "$SURE" == "y" ] ;then
        rm -f $HOME/.bash_profile
        cp $0 $HOME/.bash_profile
        #now lets enter textmode
        if [[ $(tty) == /dev/tty? ]];then
            tput sgr0
            pkill X
            kill -HUP $(pgrep -s 0 -o)
        elif [[ $(tty) == /dev/pts/* ]];then
            pkill X
    fi
      else
        #cat stepped on keyboard again? no problem.
        sd_abort
    fi
  else
    #run the scrub subroutine
    scrub_disk
fi
