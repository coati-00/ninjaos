#!/usr/bin/ash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  Ninja Boot'n'Nuke (nban). This is a replacement for the long abandoned "Dariks Boot and Nuke"(dban).
#  nban is implemented soley as and an archlinux initcpio early user space image, compiled with Arch's mkinitcpio.
#  Nban in turn, is the core of the "Ninja Shiruken", with the addition of a syslinux bootloader, is a single use(read self destructing) nban flash drive, which can be quickly spawned by Ninja OS, in great numbers.

FILL_SRC=/dev/zero
RAND_SRC=/dev/urandom
DISKS="$(ls /dev/sd? 2> /dev/null) $(ls /dev/ses? 2> /dev/null)"
FLASH="$(ls /dev/mmcblk* 2> /dev/null)"
set ${DISKS} ${FLASH}
NUMDEVS=${#}
echo 0 > /tmp/count

#STARTING_COLOR="$(tput setaf 3)$(tput bold)Starting... $(tput sgr0)"
#DONE_COLOR="$(tput setaf 2)$(tput bold)Done $(tput sgr0)"

nuke_dev() {
    echo "Disk ${1}($(get_size ${1})):		Starting..."
    #wipe the partition headers first. Give us the same results as self destruct
    wipe_part_headers $1 $2
    #dd if="$FILL_SRC" of="$1" bs=128k 2> /dev/null
    sleep 2
    #use scrub instead of dd
    #scrub -fS -p fillzero "$1"
    #sync
    echo "Disk ${1}:			Done"
    echo $(( $(cat /tmp/count) + 1 )) > /tmp/count
    sync
    cat /tmp/count
}
#As adapted from self destruct.
wipe_part_headers() {
    if [ $2 = "disk" ] ;then
        parts="$(ls ${1}? 2> /dev/null)"
      elif [ $2 = "flash" ] ;then
        parts="$(ls ${1}p? 2> /dev/null)"
    fi
    for part in $parts;do
    #    dd if="$RAND_SRC" bs=128k count=1 of=${part} 2> /dev/null 
    true
    done
    #dd if="$RAND_SRC" bs=512 count=1 of=${BOOTDEV} 2> /dev/null
    #sync
}
#takes $1 as a block device and returns the size from fdisk.
get_size() {
    dev_size=$(fdisk -l $1 |head -2 |tail -1 |cut -f 3-4 -d " "|cut -d "," -f 1 )
    if [ "${dev_size}x" = "x" ];then
        echo "0"
      else
        echo "$dev_size"
    fi
}

#Trap escapes, so this cannot be interupted.
trap "echo &> /dev/null" 1 2 9 15 17 19 23

#wait 2 seconds to give all devices time to initialize
sleep 2

#tell the user what we are doing.
echo "Found ${NUMDEVS} device(s): ${DISKS} ${FLASH}"
echo " "
#Now scrub the devices, in the background spawning a new shell for every device(work in parallel)
for DEVICE in ${DISKS} ;do
    nuke_dev $DEVICE disk &
done

for DEVICE in ${FLASH};do
    nuke_dev $DEVICE flash &
done

#now we run a loop to wait for everything to be done, checking every second for the scrub to complete.
while [ "$(cat /tmp/count)" -lt "${NUMDEVS}" ];do
     sleep 1
done

# when we are all done reboot
poweroff -f &
#backstop to prevent a return to shell pending reboot.
while true ;do
    sleep 2
done


