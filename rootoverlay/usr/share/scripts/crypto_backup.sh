#!/bin/bash
#
#  Written for the Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Script to back up GPG and OTR keys to a specified location. use as crypto_backup.sh (--restore) <backup location>
TARGET="$1"
EXIT="0"

#check if a a copy operation fails
copy_check() {
    local exit_status="$1"
    local source="$2"
    local destination="$3"
    if [ "$exit_status" -eq "0" ];then
        echo "copied $source to $destination"
      else
        echo "No such file $source"
        EXIT=$(($EXIT + 1))
    fi
}
#Do the inverse, and copy keys from a backup to the running system
crypto_restore() {
    if [ -z "$TARGET" ];then
        TARGET="$PWD"
    fi
    if [ ! -d "$TARGET"/.gnupg ] && [ ! -f "$TARGET"/.purple/otr.fingerprints ] && [ ! -f "$TARGET"/.purple/otr.private_key ] && [ ! -d "$TARGET"/.xchat/otr/ ];then
        echo "$(tput bold)$(tput setaf 1)crypto_backup.sh:$(tput sgr0) No Files to Restore"
        exit 1
      else
        echo "$(tput bold) $(tput setaf 6)+$(tput setaf 7)--$(tput sgr0) Restoring GPG and OTR Keys to $HOME"
    fi
    cp -Rfv "$TARGET"/* "$HOME"/
}
crypto_backup() {
    echo "$(tput bold) $(tput setaf 6)+$(tput setaf 7)--$(tput sgr0) Backing up GPG and OTR Keys to $TARGET"
    cp -R "$HOME"/.gnupg/ "$TARGET"
    copy_check $? "$HOME"/.gnupg/ "$TARGET"

    cp "$HOME"/.purple/otr.fingerprints "$TARGET"/.purple/otr.fingerprints
    copy_check $? "$HOME"/.purple/otr.fingerprints "$TARGET"/.purple/otr.fingerprints

    cp "$HOME"/.purple/otr.private_key "$TARGET"/.purple/otr.private_key
    copy_check $? "$HOME"/.purple/otr.private_key "$TARGET"/.purple/otr.private_key

    cp -R "$HOME"/.xcha2/otr/ "$TARGET"/.xchat/
    copy_check $? "$HOME"/.xchat2/otr/ "$TARGET"/.xchat/

    echo "$(tput bold)crypto_backup.sh:$(tput sgr0) Crypto Keys Backed Up To $TARGET ."
    exit $EXIT
}
##switches
if [ -z "$TARGET" ];then
    TARGET="$PWD"
fi
if [ "$1" = "--help" ] || [ "$1" = "-?" ] ;then
    echo "$(tput bold)crypto_backup.sh:$(tput sgr0) This script backs up(and restores) GPG and OTR keys to a target directory. If no target is given present the present working directory is used. be EXTREMELY careful"
    echo " "
    echo "	$(tput bold)usage:$(tput sgr0) crypto_backup.sh (--restore) \<back up target\>"
    echo " "
    echo "$(tput bold) Switches:$(tput sgr0)"
    echo "	$(tput bold)--restore$(tput sgr0) - Restore previously backed up crypto keys"
    exit 1
  elif [ "$1" = "--restore" ];then
    TARGET="$2"
    crypto_restore
  else
    crypto_backup
fi
