#!/usr/bin/env python3
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#   Mac & Host Scramble. This script Randomizes the system hostname and
# the MAC addresses of all detected Ethernet inferfaces. Detects interface
# type instead of guessing via naming scheme.
# This is a python re-write of the original BASH script
#
# needs python-netifaces
 
######CONFIG##########################################################
#Sets the source of random data for ethernet addresses.              #
class config:                                                        #
    RAND_SRC      = "/dev/urandom"                                   #
#The file with a list of known ethernet vendor bytes. Used with the  #
# --use-vendor-bytes option.                                         #
    MAC_LIST      = "/usr/share/eth_vendors.txt"                     #
######################################################################
 
import random
import subprocess
import sys
import os

#check if netifaces is installed
try:
    import netifaces
except:
    print("mh_scramble.py: python-netifaces is not installed, exiting")
    sys.exit(1)

class colors:
    '''pretty terminal colors'''
    reset='\033[0m'
    bold='\033[01m'
    red='\033[31m'
    cyan='\033[36m'
    lightgrey='\033[37m'
    darkgrey='\033[90m'
    lightblue='\033[94m'
    lightcyan='\033[96m'

 
def gen_mac_random():
    '''generates a completely random mac'''
    import random
    #an odd number in the first hex number is multi-cast and invalid for assignment
    a1 = random.choice("0123456789abcdef") + random.choice("02468ace")
    a2 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    a3 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    a4 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    a5 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    a6 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    
    output = a1 + ":" + a2 + ":" + a3 + ":" + a4 + ":" + a5 + ":" + a6 
    return(output)
 
def gen_mac_vid(vid_file):
    '''generates a mac with vendor bytes read from a list'''
    import random
    try:
        inFile    = open(vid_file,"r")
        fileLines = inFile.readlines()
        inFile.close()
    except:
        exit_with_error("Cannot read VID file")
        
    #delete blank lines
    num_blanks = fileLines.count("")
    for i in range(num_blanks):
        del(fileLines[fileLines.index("")])

    #get a random line from the file
    output = fileLines[ random.randrange( len(fileLines) ) ]
    #get the MAC address from the line
    output = output.split()[0]
    
    #and now for some random device bits
    a4 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    a5 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    a6 = random.choice("0123456789abcdef") + random.choice("0123456789abcdef")
    output = output + ":" + a4 + ":" + a5 + ":" + a6 
    return(output)
    
def get_eth_interfaces():
    '''return all ethernet interfaces detected in a list'''
    #all interfaces
    int_list = netifaces.interfaces()
    #remove the loopback
    del( int_list[ int_list.index('lo') ] )
    #now make a list with only ethernet addresses
    eth_list = []
    for iface in int_list:
        if netifaces.AF_LINK in netifaces.ifaddresses(iface):
            eth_list.append(iface)
    return eth_list

def random_hostname():
    try:
        new_hostname = str( subprocess.check_output(['apg', '-a0', '-n1', '-m4', '-x10', '-Mnc', '-cP34nU7w4a4S5Lt3D', '-q']) )
        new_hostname = new_hostname.strip("\\n\'")
        new_hostname = new_hostname.strip("b\'")
    except FileNotFoundError:
        exit_with_error("APG Binary Not Found")
        sys.exit(1)
    return new_hostname

def random_machine_id():
    output = ""
    for i in range(32):
        output += random.choice("0123456789abcdef")
    return output

def exit_with_error(message):
    print(colors.bold + colors.red +"!mh_scramble.py: ERROR: " + colors.reset + message)
    sys.exit(1)

def continue_with_error(message):
    print(colors.bold + colors.red +"!mh_scramble.py: WARN: " + colors.reset + message)
    return

def main():
    '''main program'''
    #start with the argument parser
    import argparse
    parser = argparse.ArgumentParser(description='''MAC and HOST scramble: Scrambles Ethernet MAC addresses, system hostname, and /etc/machine-id''')
    parser.add_argument('-s','--rescramble',help="Repeat the proccess after boot, restarts X when complete",action="store_true")
    parser.add_argument('-r','--random_vid',help="Don't Read VID bytes from file, use completely random VID bytes",action="store_true")

    depreciated = parser.add_argument_group("Depreciated","Kept for backwards compatibility, do not use")
    depreciated.add_argument('--use-vendor-bytes',help="Now the default option, depreciated, do not use",action="store_true")
    args = parser.parse_args()

    #get interface names
    iface_list      = get_eth_interfaces()
    iface_count     = len(iface_list)

    print( colors.bold + colors.cyan + "--+" + colors.lightgrey + " Mac and Host Scramble " + colors.cyan + "+--" + colors.reset)
    
    #make new MAC addresses for each interface
    iface_errors = 0
    newmac       = ""
    for iface in iface_list:
        if args.random_vid == True:
            newmac = gen_mac_random()
        else:
            newmac = gen_mac_vid(config.MAC_LIST)
        try:
            subprocess.check_call("ifconfig " + iface + " down",shell=True)
            subprocess.check_call("ifconfig " + iface + " hw ether " + newmac,shell=True)
            subprocess.check_call("ifconfig " + iface + " up",shell=True)
        except:
            continue_with_error("Cannot change MAC for " + iface)
            iface_errors += 1
            continue
        print(colors.bold + "\t" + iface + colors.reset)
        print(iface +": changed mac to " + newmac)
        #save the MAC for later, so we can cross refrence what the MAC is supposed to be if another program changes the mac, we can keep it consistant until we decide to change it again.
        outfile = open("/var/interface/"+iface,"w")
        outfile.write(newmac)
        outfile.close()
    
    #resets /etc/machine-id, used by udev to "identify machines on networks with changing MAC addresses". I do say good sir!
    machine_id = random_machine_id()
    os.chmod('/etc/machine-id',644)
    outfile = open('/etc/machine-id',"w")
    outfile.write(machine_id)
    outfile.write("\n")
    outfile.close()
    os.chmod('/etc/machine-id',444)

    #new hostname, we still use APG for pronouncable passwords
    new_hostname = random_hostname()
    #write it to file so it works
    try:
        subprocess.check_call("hostname " + new_hostname,shell=True)
    except:
        exit_with_error("Cannot set hostname, root?")
    #write /etc/hosts
    try:
        infile  = open("/etc/hosts.head","r")
        outfile = open("/etc/hosts","w")

        hosts_header = infile.read()
        hosts_text   = '127.0.0.1        localhost.localdomain    localhost ' + new_hostname + '\n'
        hosts_text  += '::1              localhost.localdomain    localhost ' + new_hostname + '\n'
        
        outfile.write(hosts_header+"\n")
        outfile.write(hosts_text)

        infile.close()
        outfile.close()
    except:
        continue_with_error("Cannot write to /etc/hosts, you need to manually add the new hostname to 127.0.0.1 in /etc/hosts")

    #Now print message and exit.
    exit_string= "mh_scramble: changed hostname to " + colors.bold + new_hostname + colors.reset + ", found " + str(iface_count) + " ethernet interfaces found, " + str(iface_count - iface_errors) + " scrambled. Scrambled /etc/machine-id. Lasts until reboot"
    print(exit_string)
    
    #if this script was called with --rescramble,
    #lets kill X which will log the system out, which will re-trigger the autologin
    #which is neccary because XFCE freaks out and stops working if the system hostname changes while running.
    if args.rescramble == True:
        subprocess.check_call("pkill X",shell=True)
    if iface_errors > 0:
	    sys.exit(2)

if __name__ == "__main__":
    main()

