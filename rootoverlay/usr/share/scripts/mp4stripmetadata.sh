#!/bin/sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#Us AtomicParsley to strip metadata from mp4 files
AtomicParsley --artwork REMOVE_ALL --metaEnema --foorbar2000Enema $*
