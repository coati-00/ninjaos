#!/bin/bash
. /usr/share/scripts/liveos_boilerplate.sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script will create a clone of the running copy Ninja OS into .img files suitable for use with makeninja.sh and ninjaforge.sh. This is essentially the inverse command. It can also duplicate straight to another disk with --dupe

#override automatic location detection in version file in case its local.(force to use system file)
OSNAME=$(awk 'NR==1{print $1}' '/var/LIVEOS_VERSION.TXT')
OSVERSION=$(awk 'NR==1{print $2}' '/var/LIVEOS_VERSION.TXT')

TARGET="$1"
MAINIMG="${OSSLUG}_${OSVERSION}.img"
BOOTSECTOR="ninjabootsector${OSVERSION}.img"
DUPE="FALSE"
PACKAGE="FALSE"
EXIT=0

liveos_dupe() {
    #check if target is a block device
    if [ ! -b ${TARGET} ] || [ ${TARGET} == ${BOOTDEV} ] ;then
        echo "$(tput bold)clonenina.sh:$(tput sgr0) cannot duplicate ${OSNAME} to ${TARGET}, ${TARGET} is not a block device, or it is the OS block device."
        EXIT=$(($EXIT+1))
        return
    fi

    #Partition the new drive for Ninja OS
    /usr/share/scripts/ninjaforge.sh --partonly ${TARGET}
    EXIT=$(($EXIT + $?))

    echo "$(tput bold)cloneninja.sh:$(tput sgr0) Duplicating Running(${OSNAME} ${OSVERSION}) system to ${1}. This may take a while..."
    #copy boot sector
    echo "	${BOOTDEV}->${TARGET}:"
    sudo dd if=${BOOTDEV} bs=440 count=1 2> /dev/null | pv -s 440| sudo dd of=${TARGET} bs=440 count=1 &> /dev/null
    EXIT=$(($EXIT + $?))
    sync
    #copy operating system
    echo "	${BOOTPART}->${TARGET}1:"
    sudo dd if=${BOOTPART} bs=128k 2> /dev/null |pv -B 128k -s 1222639616 | sudo dd of=${TARGET}1 bs=128k &> /dev/null
    EXIT=$(($EXIT + $?))
    sync

}
liveos_clone() {
    echo "$(tput bold)cloneninja.sh:$(tput sgr0) Cloning Running($OSNAME $OSVERSION) system into .img files. This may take a while..."
    #now lets grab the operating system with dd
    echo "	${BOOTPART}->${MAINIMG}:"
    sudo dd if=${BOOTPART} bs=128k 2> /dev/null |pv -B 128k -s 1222639616 | dd of="${TARGET}/${MAINIMG}" bs=128k &> /dev/null
    EXIT=$(($EXIT + $?))
    #now the boot sector
    echo "	${BOOTDEV}->$BOOTSECTOR:"
    sudo dd if=${BOOTDEV} bs=440 count=1 2> /dev/null | pv -s 440| dd of="${TARGET}/${BOOTSECTOR}" bs=440 count=1 &> /dev/null
    EXIT=$(($EXIT + $?))
    sync
}

#This needs to be run after liveos_clone and assumes the .img files exist
liveos_package() {
    if [[ ! -f ${TARGET}/${MAINIMG}  ||  ! -f ${TARGET}/${BOOTSECTOR} ]];then
        echo "${0}: can't find $OSNAME image and/or bootsector to package, aborting"
        EXIT=$(($EXIT + 1))
        return
    fi
    echo "$(tput bold)cloneninja.sh:$(tput sgr0) Packing .img, and install files into ${OSNAME}-${OSVERSION}.zip" 
    mkdir "${TARGET}"/${OSNAME}-${OSVERSION}
    mv "${TARGET}/${MAINIMG}" ${OSNAME}-${OSVERSION}/
    mv "${TARGET}/${BOOTSECTOR}" ${OSNAME}-${OSVERSION}/
    cp /usr/share/scripts/install/* ${OSNAME}-${OSVERSION}/
    cp /var/LIVEOS_VERSION.TXT ${OSNAME}-${OSVERSION}/
    cd "${TARGET}"/${OSNAME}-${OSVERSION}
    zip -q ./${OSNAME}-${OSVERSION}.zip *
    mv ${OSNAME}-${OSVERSION}.zip ..
    rm -rf "${TARGET}"/${OSNAME}-${OSVERSION}
    sync
}

clone_help() {
   echo "$(tput bold)cloneninja.sh:$(tput sgr0) Clones the currently running ${OSNAME} system into .img files suitable for use with the forge command. if run by itself if will make the images into the current dirrectory. if run with a directory path, it will use that instead."
    echo ""
    echo "usage: cloneninja.sh <optional-target-directory>"
    echo ""
    echo "	$(tput bold)Options:$(tput sgr0)"
    echo " $(tput bold)--dupe$(tput sgr0)	Copies dirrectly to another device(block) without making .img files"
    echo " "
    echo " $(tput bold)--package$(tput sgr0)	Makes a complete re-distributable .zip file with forge scripts, README, and licensing files"
    exit 1
}

#help and options checker.

if [ "$1" = "--dupe" ];then
    TARGET="$2"
    DUPE="TRUE"
  elif [ "$1" = "--help" ] || [ "$1" = "-?" ]; then
    clone_help
  elif [ "$1" = "--package" ];then
    PACKAGE="TRUE"
    TARGET="$2"

  elif [ ! -d "${TARGET}" ] && [ ! -z "${TARGET}" ];then
    echo "$(tput bold)$(tput setaf 1)cloneninja.sh:$(tput sgr0) invalid dirrectory see cloneninja.sh --help"
    exit 1
fi
if  [ -z "${TARGET}" ];then
    TARGET="$PWD"
fi
#main part, where we decide on action to take, either dupe to another USB stick, or clone to files.
if [ "${DUPE}" == "TRUE" ];then
    liveos_dupe ${TARGET}
  else
    liveos_clone ${TARGET}
    if [ "${PACKAGE}" == "TRUE" ];then
        liveos_package
    fi
fi

#Exit. Check status, if there are failures, print a fail message, otherwise print a success message.
if [ "$EXIT" -eq "0" ];then
  echo "$(tput bold)$(tput setaf 6)DONE!$(tput sgr0)"
  exit 0
 else
  echo "$(tput bold)$(tput setaf 1)!FAIL!:$(tput sgr0) There was an error copying(script threw a code somewhere)"
  exit $EXIT
fi
