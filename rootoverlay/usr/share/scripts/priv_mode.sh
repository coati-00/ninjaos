#!/bin/bash
#
#  Written for the Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  This script should run "Privacy" mode. Random MACs, Random hostname, and start tor(the onion router) and polipo

userhome=/home/user/
# scramble ethernet MACs and the system hostname
/usr/share/scripts/mh_scramble.py --use-vendor-bytes

##Add vidalia(tor controlor) to autostart, and add Privacy mode desktop icons

cp /usr/share/applications/notify-privmode.desktop /home/user/.config/autostart/
cp /usr/share/applications/vidalia.desktop /home/user/Desktop/
cp /usr/share/scripts/rescramble.desktop /home/user/Desktop/
chown user:users /home/user/.config/autostart/notify-privmode.desktop
chown user:users /home/user/Desktop/vidalia.desktop
chown user:users /home/user/Desktop/rescramble.desktop
chmod +x /home/user/Desktop/vidalia.desktop

#cp /usr/share/scripts/i2p_daemon_controller.desktop /home/user/Desktop/
#chown user:users /home/user/Desktop/i2p_daemon_controller.desktop
