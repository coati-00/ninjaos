#!/bin/bash
. /usr/share/scripts/liveos_boilerplate.sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  Checks system components outside of the squashfile for signs of damage and/or tampering
#    Now with uses GNUPG signature checks instead of simple hash tests.

#override the boilerplate because we need static versioning
OSNAME=$(awk 'NR==1{print $1}' '/var/LIVEOS_VERSION.TXT')
OSVERSION=$(awk 'NR==1{print $2}' '/var/LIVEOS_VERSION.TXT')
#colors for pass and fail
OK_COLOR="$(tput bold)$(tput setaf 6)OK!$(tput sgr0)"
FAIL_COLOR="$(tput bold)$(tput setaf 1)!FAILS!$(tput sgr0)"
EXIT=0
KEYNAME=$(gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --list-keys |head -3 | tail -1|cut -d "/" -f 2 |cut -d " " -f 1)

echo "$(tput bold) $(tput setaf 6)--+$(tput setaf 7)$OSNAME Integrity Check$(tput setaf 6)+-- $(tput sgr0)"
echo "    GPG Key: $(tput bold)$(tput setaf 2)${KEYNAME}$(tput sgr0)"
echo ""

# int_errors is a running log of fail messages from GPG, while temp_init is the current error message. we display these at the end to the user
touch /tmp/int_errors
touch /tmp/temp_int

#first we check the boot sector
sudo dd if=$BOOTDEV of=/tmp/bootsectorimage bs=440 count=1 > /dev/null 2>&1
gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /var/lib/misc/bootsector.sig /tmp/bootsectorimage 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Boot Sector				$OK_COLOR"
 else
  echo "Boot Sector				$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "Bootsector:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi
sudo rm /tmp/bootsectorimage

gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /var/lib/misc/vesamenu.c32.sig /boot/isolinux/vesamenu.c32 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Boot Menu				$OK_COLOR"
 else
  echo "Boot Menu				$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "Bootmenu:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

#Now lets check the kernel
KERNELNAME=$(cat /boot/kernelname)
gpg --verify /var/lib/misc/kernel.sig /boot/$KERNELNAME 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "KERNEL: $KERNELNAME	$OK_COLOR"
 else
  echo "KERNEL: $KERNELNAME	$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "Kernel": >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

#check intel microcode initial ramdisk, loaded beforehand.
gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /var/lib/misc/intel-ucode.img.sig /boot/intel-ucode.img 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Intel microcode				$OK_COLOR"
 else
  echo "Intel microcode				$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "initramdisk:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

#make sure the version file hasn't been tampered with
gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /var/lib/misc/LIVEOS_VERSION.TXT.sig /var/LIVEOS_VERSION.TXT 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Version File				$OK_COLOR"
 else
  echo "Version File				$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "verfile:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

#Once we can confirm LIVEOS_VERSION hasn't been tampered with, we can trust the SHA256 sum
BOOTIMAGE_LOC=$(awk 'NR==2{print $1}' /var/LIVEOS_VERSION.TXT)
BOOT_VERSION=$(awk 'NR==1{print $0}' /var/LIVEOS_VERSION.TXT)
BOOTIMAGE_CURRENT=$(shasum -a 256 $BOOTIMAGE_LOC | cut -d " " -f 1)
BOOTIMAGE_SAVED=$(awk 'NR==2{print $2}' /var/LIVEOS_VERSION.TXT)
if [ $BOOTIMAGE_CURRENT == $BOOTIMAGE_SAVED ]; then
  echo "+++ Boot Menu Background File		$OK_COLOR"
 else
  echo "+++ Boot Menu Background File		$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
fi
echo "----------------"
#below this line are files checked from /boot/sigs, a read-write area outside the main body of the squashfile(and thus easily changable). This is presumed to be safe, because they are checked against a public key that *is* conainted in the squashfile. This would not be acceptable with SHA256

gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /boot/sigs/larch.img.sig /boot/larch.img 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Initial Ramdisk				$OK_COLOR"
 else
  echo "Initial Ramdisk				$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "initramdisk:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

#now the bootloader in /boot/isolinux/ldlinux.sys
gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /boot/sigs/ldlinux.sys.sig /boot/isolinux/ldlinux.sys 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Boot Loader				$OK_COLOR"
 else
  echo "Boot Loader				$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "Bootloader:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /boot/sigs/mods.sqf.sig /.livesys/medium/larch/mods.sqf 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Modules Squash File			$OK_COLOR"
 else
  echo "Modules Squash File			$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "mods.sqf:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

echo "Checking Main System SquashFS file, this might take a while..."
gpg --no-default-keyring --keyring /usr/share/ninja_pubring.gpg --verify /boot/sigs/system.sqf.sig /.livesys/medium/larch/system.sqf 2> /tmp/temp_int
if [ "$?" -eq "0" ];then
  echo "Main Squash File			$OK_COLOR"
 else
  echo "Main Squash File			$FAIL_COLOR"
  EXIT=$(($EXIT + 1))
  echo "system.sqf:" >> /tmp/int_errors
  cat /tmp/temp_int >> /tmp/int_errors
fi

#Now lets exit, and give the user a summary of errors found
if [ $EXIT -eq 0 ];then
  EXIT_COLOR="6"
 else
  EXIT_COLOR="1"
fi
echo " "
echo "This disk identifies itself as $(tput bold)$BOOT_VERSION$(tput sgr0). You should see this name and version number at the boot menu."
echo " "
echo "	There were $(tput bold)$(tput setaf $EXIT_COLOR)$EXIT$(tput sgr0) failure(s)"
echo " "
#print error lot at the end
cat /tmp/int_errors
echo " "
echo "Any failures likely indicate either drive damage or tampering, in either case replacing the medium or restoring the system partition from a good back up should be done immediately"
#remove error log
rm /tmp/int_errors
rm /tmp/temp_int
exit $EXIT
