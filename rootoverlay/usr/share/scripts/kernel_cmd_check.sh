#!/bin/bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  This script proccesses Ninja OS specific items entered on the kernel command line. It is called by systemd at startup. options "nox" and "xconfigure" are in ~/.bash_profile
EXIT=0
# privacy mode
if [[ $(cat /proc/cmdline) == *privmodetrue* ]];then
    /usr/share/scripts/priv_mode.sh
    EXIT=$(($EXIT + $?))
fi
## Lets check if self destruct was selected at start up. zeroize is now a synonym for self-destruct
if [[ $(cat /proc/cmdline) == *selfdestruct* ]] || [[ $(cat /proc/cmdline) == *zeroize* ]]|| [[ $(cat /proc/cmdline) == *zzz* ]];then
    rm /home/user/.bash_profile
    cp /usr/share/scripts/liveos_sd.sh /home/user/.bash_profile
fi

# Check for camouflage arguments
if [[ $(cat /proc/cmdline) == *camo-winxp* ]];then
    /usr/share/scripts/xfce4-camo.sh winxp
    EXIT=$(($EXIT + 1))
  elif [[ $(cat /proc/cmdline) == *camo-win7* ]];then
    /usr/share/scripts/xfce4-camo.sh win7
    EXIT=$(($EXIT + 1))
  elif [[ $(cat /proc/cmdline) == *camo-win8* ]];then
    /usr/share/scripts/xfce4-camo.sh win8
    EXIT=$(($EXIT + 1))
  elif [[ $(cat /proc/cmdline) == *camo-osx* ]];then
    /usr/share/scripts/xfce4-camo.sh osx
    EXIT=$(($EXIT + 1))
fi

exit $EXIT
