The following scripts have been written for use with Ninja OS, they are available in /usr/share/scripts/:

Converting, Cleaning, Scrubbing Scripts -
---------------
nrg2iso.sh - batch converts proprietary nero images to standard .iso files
pngstrip.sh - uses pngcrush to strip metadata off .png images (formerly pngbatch.sh)
mp4stripmetadata.sh - Strips metadata off mp4 videos with AtomicParslsey (work in progress)
3gpstripmetadata.sh - Strips metadata off 3gpp videos with AtomicParlsey (work in progress)
mat_notify.sh/srm_notify.sh - called from within the file manager. makes use of libnotify when standard output and error cannot be seen from not running in a console

EXPERIMENTAL - diskscrub.sh - after zerofill/rand fill, uses hdparm to destroy recovery information. Be warned, the hdparm --security-erase is still experimental and may cause kernel panic.

Misc
----
liveos_integritycheck.sh - hash sums some important files and compares them with saved defaults.
getbshash.sh - gets the hash of the first 440 bytes of the bootsector of a block device(512 total).
disabled.sh - error message to inform the user that a particular script has been disabled pending a re-write. this is a temprorary script that will last only as long as NinjaOS is pre-release.(beta)
liveos_sd.sh - self destruct mode. will overwrite the entire flash drive. use with care.
liveos_boilerplate.sh - sets common LiveOS bash variables. add ". /usr/share/scripts/liveos_boilerplate.sh " to the top of your scripts.
crypt_backup.sh - backs up OTR and GPG keys to a directory, or restores them, from a previous backup.
parachute.sh - copies panic_shutdown.sh and busybox to /tmp/
firewall_gui_control.sh - wrapper to enable and disable iptables with systemctl, and also use libnotify to inform the user.
bootanuke.sh - the script behind Ninja Boot'N'Nuke. This is not run directly, but compiled into the nban initcpio image.
/install - all the files that go into the Ninja OS installer. to include portable versions of Forge.

Clone and Forge Scripts
-----------------------
makeninja.sh - makes a ninja os usb stick from image files.
ninjaforge.sh - deletes and repartitions a usbstick then runs makeninja.sh to copy NinjaOS files
cloneninja.sh - clones the running system into image files

Privacy Mode Scripts
--------------------
priv_notify.sh - informs user that privacy mode is activated via notify-send. checks if privacy mode is activated before using notify-send to send a pop up to the desktop
priv_mode.sh - script is called from /etc/rc.local which runs if privacy mode is enabled.
mh_scramble.py - mac and host name scrambling. called from priv_mode.sh. to use from a running system use mh_scramble.sy --rescramble.
rescramble_gui.sh - runs mh_scramble --rescramble and uses notify-send to post a message to the GUI deskop.
rescramble.desktop - desktop icon that calls rescramble_gui.sh. it gets copied to the desktop on privacy mode boot.

System
------
umount_crypto.sh - Runs at shutdown to unmount encrypted partitions, clearing keys from memory before shutdown.
multi_mon.sh - runs at startup after XFCE is loaded to handle multi-monitor setups, with xrandr
kernel_cmd_check.sh - runs at start up from systemd, proccesses options on the kernel command line(from /proc/cmdline)
panic_shutdown.sh - gets copied to /tmp/auto_shutdown/ on boot along with a custom compiled version of busybox. emergency shutdown script that works he system if the boot medium is removed.
drive_watch.sh - watches to make sure the USB stick running Ninja OS stays plugged in. Otherwise issues a ACPI reboot.
xfce4-camo.sh - camoflauge XFCE to look like other various OSs.(not working yet)

Third Party
-----------
check_arp.py - watches an IP address for arp address changes. watch for arp
poisoning. FreeBSD licensed. Written by Jack @ NYI.
