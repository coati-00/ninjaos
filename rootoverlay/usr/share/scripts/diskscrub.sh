#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Block device shredder. use as diskscrub.sh <options> <device filename>
# See diskscrub.sh --help
RANDSRC=/dev/urandom
ZEROSRC=/dev/zero
PASSES=2
DISK=""
exit=0

#This subroutine scrubs recovery data off Solid State Drives. scrub_ssd <target disk> <arbitrarty password string>
#based off: https://wiki.archlinux.org/index.php/SSD_Memory_Cell_Clearing
scrub_sata() {
  #lets get parameters
  local disk=$1
  local password=$2
  local rtime=$(sudo hdparm -I $disk|grep SECURITY\ ERASE\ UNIT| cut -d \  -f 1)
  local etime=$(sudo hdparm -I $disk|grep SECURITY\ ERASE\ UNIT| cut -d \  -f 6)
  [ -z $2 ] && password=p4ssw0rd
  #test to see if drive is "frozen", if not 
  local isfrozen=$(sudo hdparm -I $disk | grep -i frozen)
  if [ $isfrozen == "	not	frozen" ];then
    #sets a temporary password, which will be overwritten anyway in the wipe
    sudo hdparm --user-master u --security-set-pass $password $disk
    exit=(($exit + $?))
    echo "$(tput bold)diskscrub.sh:$(tput sgr0) We are now wiping back up data off $disk, this should take around $etime, be patient"
    #now lets wipe backup data
    sudo hdparm --user-master u --security-erase-enhanced $password $disk
    exit=(($exit + $?))
  else
    echo "$(tput bold)diskscrub.sh:$(tput sgr0) SSD is Frozen, not scrubing back-up information, unfreeze, and retry"
    exit 1
  fi
}

#fills target with all zeros 0x00, zerofill <block device> <Name of repition>
zero_fill() {
  local disk=$1
  local rep=$2
  echo "$(tput bold)diskscrub.sh:$(tput sgr0) Starting zerofill of $disk \($rep\), be patient this might take a while..."
  sudo time dd if=$ZEROSRC of=$disk bs=128k 
  exit=(($exit + $?))
  echo "$(tput bold)diskscrub.sh:$(tput sgr0) $rep Zerofill \($rep\) complete"
}
#error handling subroutine
diskscrub_error() {
  if [ $1 == "notarget" ];then
    echo "$(tput bold)diskscrub.sh:$(tput sgr0) No Target! enter device file to scrub see diskscrub.sh --help for more information"
  elif [ $1 == "badpattern" ];then
          echo "$(tput bold)diskscrub.sh:$(tput sgr0) Unrecognized Pattern"
  fi
  exit 1
}

## start main script ##

#first we check for --options and ensure a block device is specified.
if [ -z $1 ];then
  diskscrub_error notarget
 elif [ $1 = "--help" ] || [ $1 = "-?" ]; then
  echo "$(tput bold)diskscrub.sh:$(tput sgr0) This shell script securely wipes data off of disk and flash drives. Defaults to two passes of zerofil followed up by a SATA hdparm backup data wipe."
  echo ""
  echo " " $(tput bold)Usage: $(tput sgr0) diskscrub.sh "<options> <device file>"
  echo "$(tput bold)Options:$(tput sgr0)"
  echo "	$(tput bold)-p$(tput sgr0), $(tput bold)--pattern$(tput sgr0)	The technique and amount of passes to use. use a number for zerofill, or use rN for N number of random passes. So far only zerofill is implemented. Default is 2 zerofill passes."
  echo " "
  exit 1
 #grab information of technique to use.
 elif [ $1 = "--pattern" ] || [ $1 = "-p" ];then
    DISK=$3
    if [ -z "$DISK" ];then
      diskscrub_error notarget
     elif [ "$2" -eq "$2" 2> /dev/null ]; then
      PASSES=$2
     else
      discrub_error badpattern
    fi 
 else
  DISK=$1
fi

#first we fill with zeros
I=$PASSES
pass="1"
while [ $I -gt "0" ];do
  zero_fill $DISK $pass
  I=$(expr $I - 1 )
  pass=$(expr $pass + 1)
done

#scrubs drive recovery data with SATA commands
scrub_sata $DISK

if [ "$exit" != "0" ];then
  echo "$(tput bold)$(tput setaf 1)diskscrub.sh:$(tput sgr0) Script threw a code $(tput bold)$(tput setaf 1)!FAIL!$(tput sgr0)"
    exit $exit
  else
    exit 0
fi

