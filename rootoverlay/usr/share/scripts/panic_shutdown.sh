# no shebang, because it won't work, assume /bin/bash is running
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script acts as an emergency shutdown, it can shutdown the system when the boot media is unavailable. It calls all of its commands from a hard linked stripped down version of busybox loaded into ram at boot, along with this script.

#If for some reason this takes more than 5 seconds, reboot anyway. safeguard against being "stuck"
dead_mans_switch() {
    /tmp/emergency_bin/busybox sleep 5
    sudo /tmp/emergency_bin/busybox reboot -f
}
dead_mans_switch &

#sync all files
/tmp/emergency_bin/busybox sync

#umount all drives
/tmp/emergency_bin/busybox umount -fia

# reboot
/tmp/emergency_bin/busybox reboot -f

