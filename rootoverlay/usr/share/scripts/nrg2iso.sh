#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script utilizes nrg2iso in batch form use as nrg2iso.sh <filenames>
declare -a FILELIST
FILELIST=("$@")
LINES=${#FILELIST[@]}
TEMPFILE=""

nrgiso_proccess() {
  local infile="$@"
  local outfile="${infile%.nrg}".iso
  local exit_local=0
  if [ -z "$infile" ];then
    echo "$(tput bold)pngstrip.sh$(tput sgr0): $infile DOES NOT EXIST."
    return 1
  fi
  nrg2iso "$infile" "$outfile"
  exit_local="$?"
  srm -ll "$infile"
  return $exit_local
}

#sanity check
if [ -z "$FILELIST" ];then
  echo "$(tput bold)nrg2iso.sh$(tput sgr0): you need to specify at least one .nrg file"
  exit 1
 elif [ "$FILELIST" = "--help" ];then
  echo "$(tput bold)nrg2iso.sh$(tput sgr0): converts propriertary nero disk image .nrg files to standard .iso files in batch. uses the binary nrg2iso as a backend and respects posix style wildcards like * and ? "
 exit 1
fi
I=$(($LINES - 1))
FILE=""
while [ "$I" -ge "0" ];do
  $FILE=${FILELIST[$I]}
  nrgiso_proccess "$FILE"
  EXIT=$(($EXIT + $?))
  true $(($I--))
done

if [ "$EXIT" == "0" ];then
  exit 0
 else
  echo "$(tput bold)nrg2iso.sh$(tput sgr0): threw a code somewhere: $(tput bold)$(tput setaf 1)!FAIL!:$(tput sgr0)"
  exit $EXIT
fi
