#!/bin/bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script will enable or disable iptables rules. They are enabled by default
ACTION=${1,,}
EXIT="0"

##check input
if [ "$1" = "--help" ] || [ "$1" = "-?" ] ;then
    ACTION=help
  elif [ -z $1 ];then
    echo "$0"": no action specified"
    exit 1
  elif [ "$ACTION" != "help" ] && [ "$ACTION" != "start" ] && [ "$ACTION" != "stop" ] && [ "$ACTION" != "status" ];then
    echo "$0"": $ACTION is an invalid action"
    exit 1
fi

if [ "$ACTION" == "start" ];then
    sudo systemctl start iptables
    EXIT="$?"
    notify-send "Enabling Firewall" "Loading IPTables firewall rules" --icon=security-high
  elif [ "$ACTION" == "stop" ];then
    sudo systemctl stop iptables
    EXIT="$?"
    notify-send "Disabling Firewall" "Clearing IPtables firewall rules" --icon=security-low
  elif [ "$ACTION" == "help" ];then
    echo "$(tput bold)firewall_gui_control.sh:$(tput sgr0) This script wraps enabling and disabling of IPTables firewall rules for the gui, for use in menus. starts and stops Iptables along with using notify-send to inform the user of the action via popup. Use as:"
    echo ""
    echo " /usr/share/scripts/firewall_gui_control [start|stop|status|help]"
fi
