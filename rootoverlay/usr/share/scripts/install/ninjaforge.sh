#!/bin/bash
. ./liveos_boilerplate.sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# The script is to automaticly make a new Ninja OS USB Stick on specified block device (e.g. /dev/sdb) This should work on any read/write medium however. This will erase the entire drive in the proccess.(leaving a clean NinjaOS install). The USB Stick needs be at least 1GB for this to work. Ideally it should be at least 2GB to leave room for user data. Needs both Ninja OS

## config - edit these to the path of your makeninja.sh and partninja.sh scripts. defaults from Ninja OS
MAKENINJA="./makeninja.sh"
MAINIMG="${OSSLUG}_${OSVERSION}.img"
BOOTSECTOR="ninjabootsector${OSVERSION}.img"
TARGET="$1"
EXIT=0
PARTONLY="FALSE"

#partitioning now as a function, partninja.sh now depreciated
part_ninja() {
    local target="$1"
    local disksize="0"
    #old size versions < 0.8.x 
    #local mindisksize="976562"
    #new size 1.2 GB
    local mindisksize="1195008"
    local warndisksize="1953125"

    #Lets Check to make sure the disk is big enough first
    disksize=$(sudo sfdisk -s $target)
    if [ "$disksize" -lt "$mindisksize" ];then
        echo "(tput bold)ninjaforge.sh:$(tput sgr0) $target is less than 1GB, and cannot be set up for use with $OSNAME. Quitting..."
        exit 1
      elif [ "$disksize" -lt "$warndisksize" ];then
        echo "$(tput bold)ninjaforge.sh:$(tput sgr0) $target has more than 1GB but less than 2 GB of disk space. Script will continue, but results will be sub-optimal."
    fi

    echo "$(tput bold)ninjaforge.sh:$(tput sgr0) Setting up partitions in $target for $OSNAME..."
    #Start with making sure all file systems on device are unmounted.
    sudo umount ${target}? &> /dev/null
    #Lets wipe and remake the parition now
    sudo dd if=/dev/zero of=$target bs=128k count=1 &> /dev/null
    sync
    sudo parted -s $target mklabel msdos
    #lets make a 1GB bootable partition
    sudo parted -s $target mkpart primary 0 1225 &> /dev/null
    sudo parted -s $target set 1 boot on
    #Now make a second partition that fills the rest of the disk
    sudo parted -s $target -- mkpart primary 1225 -1 &> /dev/null
    sync
    echo "$(tput bold)$(tput setaf 6)DONE!$(tput sgr0)"
    return 0
}
get_target_console() {
    TARGET="none"
    echo "$(tput bold)ninjaforge.sh:$(tput sgr0) Please specificy target block device to install $OSNAME(in the format of /dev/sdX):"
    while [ ! -b $TARGET ];do
        read -p path: TARGET
        [ ! -b $TARGET ] && echo "$(tput bold)ninjaforge.sh:$(tput sgr0) $TARGET is not a valid block device, please try again in the format of (/dev/sdX):"
    done
    return
}
##Help and error checking routine
if [ -z "$1" ];then
    get_target_console
  elif [ "$1" = "--partonly" ];then
    PARTONLY="TRUE"
    TARGET=$2
  elif [ "$1" = "--help" ] || [ "$1" = "-?" ] ;then
    echo "$(tput bold)ninjaforge.sh:$(tput sgr0) The script is to automaticly make a new $OSNAME USB Stick on specified block device (e.g. /dev/sdb) This should work on any read/write medium however. This will erase the entire drive in the proccess.(leaving a clean $OSNAME install). The USB Stick needs be at least 1GB for this to work. Ideally it should be at least 2GB to leave room for user data. In order, it will:"
    echo ""
    echo " 1. Erase all partitioning information "
    echo " 2. Create a new MBR and a 1 GB Parition "
    echo " 3. Mark the new 1GB parition bootable "
    echo " 4. Make a blank partition that fills the rest of the drive "
    echo " 5. Use UNIX \"dd\" to copy the system to the first partition "
    echo " 6. Use UNIX \"dd\" to copy the bootsector to the MBR "
    echo ""
    echo "you may use the --partonly option to perform steps 1 through 4 only"
    echo ""
    echo "use as ninjaforge (--partonly) block device"
    exit 1
fi

#check the target and files
if [ ! -b "$TARGET" ];then
    echo "$(tput bold)$(tput setaf 1)ninjaforge.sh:$(tput sgr0) $TARGET is not a block device, quitting..."
    exit 1
  elif [ ! -f "$MAINIMG" -a "$PARTONLY" != "TRUE" ];then
    echo "$(tput bold)$(tput setaf 1)ninjaforge.sh:$(tput sgr0) $MAINIMG not found in local directory, quiting..."
    exit 1
  elif [ ! -f "$BOOTSECTOR" -a "$PARTONLY" != "TRUE" ];then
    echo "$(tput bold)$(tput setaf 1)ninjaforge.sh:$(tput sgr0) $BOOTSECTOR not found in local directory, quitting..."
    exit 1
fi
echo "$(tput bold) $(tput setaf 6) --+$(tput setaf 7)$OSNAME System Forge$(tput setaf 6)+-- $(tput sgr0 )"

#If --partonly is set, we only partition, otherwise we partition then copy the system
if [ "$PARTONLY" == "TRUE" ];then
    part_ninja $TARGET
    EXIT=$(($EXIT + $?))
    exit $EXIT
  else
    part_ninja $TARGET
    EXIT=$(($EXIT + $?))
    $MAKENINJA $TARGET
    EXIT=$(($EXIT + $?))
fi

if [ "$EXIT" -ne "0" ];then
    echo "$(tput bold) $(tput setaf 1)ninjaforge.sh:$(tput sgr0) child script threw an error. $(tput bold)$(tput setaf 1)!FAIL!$(tput sgr0)"
    exit $EXIT
  else
    echo "$(tput bold)ninjaforge.sh:$(tput sgr0) Finnished making $OSNAME on $TARGET. $(tput bold)$(tput setaf 6)SUCCESS!$(tput sgr0)"
    exit 0
fi
