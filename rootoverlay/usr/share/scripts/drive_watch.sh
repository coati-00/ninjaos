#!/bin/bash
. /usr/share/scripts/liveos_boilerplate.sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script runs at start up, stays resident and watches for the OS drive to be unplugged. If so it shuts the system down.

#trap "/tmp/emergency_bin/panic_shutdown.sh" 1 2 9 15 17 19 23
trap "/tmp/emergency_bin/busybox reboot -f" 1 2 9 15 17 19 23

while [ -b $BOOTDEV ];do
     /tmp/emergency_bin/busybox sleep 0.333
done

#reboot the system.
#/tmp/emergency_bin/panic_shutdown.sh
/tmp/emergency_bin/busybox reboot -f
