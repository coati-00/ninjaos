#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  This script does batch removal of png metadata using pngcrush via pngstrip.sh
#  usefull for usage with */? wildcards or just name multiple files
declare -a FILELIST
FILELIST=("$@")
LINES=${#FILELIST[@]}
EXIT=0

png_strip() {
  local INFILE="$@"
  local TMPFILE=/tmp/"$INFILE"-strip
  local exit_local=0
  if [ -z "$INFILE" ];then
    echo "$(tput bold)pngstrip.sh$(tput sgr0): $INFILE DOES NOT EXIST."
    return 1
  fi
  pngcrush -rem alla -rem text $INFILE $TMPFILE
  exit_local="$?"
  srm -ll "$INFILE"
  cp "$TMPFILE" "$INFILE"
  scrub -rfS -p random "$TMPFILE" &> /dev/null
  return $exit_local
}

if [ -z "$FILELIST" ];then
  echo "$(tput bold)pngstrip.sh$(tput sgr0): you need to specify at least one .png file"
  exit 1
 elif [ "$FILELIST" = "--help" ];then
  echo "$(tput bold)pngstrip.sh$(tput sgr0): this script strips png metadata, use as pngstrip.sh file1 file2 file3 etc.."
 exit 1
fi

#Loop to use pngstrip.sh on every file in order.
I=$(($lines - 1))
FILE=""
#for $I in ${filelist[@]};do
while [ "$I" -ge "0" ];do
  $FILE=${FILELIST[$I]}
  png_strip "$FILE"
  EXIT=$(expr $EXIT + $?)
  true $(($I--))
done

if [ "$EXIT" == "0" ];then
  exit 0
 else
  echo "$(tput bold)pngstrip.sh$(tput sgr0): threw a code somewhere: $(tput bold)$(tput setaf 1)!FAIL!:$(tput sgr0)"
  exit $EXIT
fi

