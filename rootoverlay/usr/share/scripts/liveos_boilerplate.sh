#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Common boilerplate to use common LiveOS $VARIABLES in scripts.
# add ". /usr/share/scripts/liveos_biolerplate.sh" to the top of your script.

# Autodetect location of LIVEOS_VERSION.TXT. it can be in current directory, $HOME, or /var/, in that order.

VERSION_FILE="/var/LIVEOS_VERSION.TXT"
if [ -f "${PWD}/LIVEOS_VERSION.TXT" ];then
    VERSION_FILE="${PWD}/LIVEOS_VERSION.TXT"
  elif [ -f "${HOME}/LIVEOS_VERSION.TXT" ];then
    VERSION_FILE="${HOME}/LIVEOS_VERSION.TXT"
fi
# $OSNAME and $OSVERSION are the name and version of the Live OS, as read from /var/LIVEOS_VERSION.TXT. $OSSLUG is $OSNAME, unixified, e.g. no spaces no special characters all lowercase to be used for filenames and commands.

# $BOOTDEV gives the name of the block device that the Live OS booted from, and $BOOTPART is the name of the partition on that device.

OSNAME=$(awk 'NR==1{print $1}' "$VERSION_FILE")
OSVERSION=$(awk 'NR==1{print $2}' "$VERSION_FILE")
OSSLUG=${OSNAME,,}

BOOTPART=$(mount |grep /boot |cut -d " " -f 1)
BOOTDEV=${BOOTPART:0:$((${#BOOTPART}-1))}
