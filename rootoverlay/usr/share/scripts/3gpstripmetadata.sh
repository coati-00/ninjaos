#!/bin/sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Use Atomic Parsley to strip off all metadata from 3gp videos
AtomicParsley --3gp-title "" --3gp-author "" --3gp-performer "" --3gp-genre "" --3gp-description "" --3gp-copyright "" --3gp-album "" --3gp-year "" --3gp-rating "" --3gp-classification "" --3gp-keyword "" --3gp-location "" $*
